{-# LANGUAGE RankNTypes,OverloadedStrings,ExistentialQuantification,DeriveGeneric,TemplateHaskell, GADTs,TypeSynonymInstances, FlexibleInstances #-}
module Init where

import SFML.Audio
import SFML.Graphics
import SFML.Window
import SFML.System.Time

import GHC.Generics

import Control.Lens hiding (transform)
import Control.Monad
import Control.Concurrent.Async
import Control.Concurrent.MVar
import Linear.V2

import Foreign.Ptr (nullPtr)
import SFML.SFException

import qualified Data.HashMap.Strict as DHS
import Data.HashMap.Strict((!))
import qualified Data.HashTable.IO as HIO
-- import Data.Text
import Data.Hashable

import Data.IORef

import Lib
import Conf
import View
import GameText
import GameData
import Animations
import UI
import Game

import App
-- forgive me for the magic numbers
mkMenuEls :: [(UICoord,Int -> IO GUI)] -> [(UICoord,IO GUI)]
mkMenuEls stuff = mkMenuEls' 0 stuff        
mkMenuEls' _ [] = []             
mkMenuEls' n ((crd,mkEl):xs) = (crd, newEl):(mkMenuEls' (n+1) xs)
  where
    newEl = do
      el <- mkEl n
      wordSize <- (el^.uiLabel) >?>= (0 :: Float) $ \l -> do
        return $  fromIntegral $ (`div` 2) $ (length  $ l^.lText) * maybeOr (l^.cSize) 30
      return $ el {uiSelect = cursorInRange crd wordSize (wordSize/2) }


testMenu :: GameEnv -> IO (GMenu)
testMenu = basicMenu [ 
  ((V2 (V2 0 1) (V2 0 1)),  mkPlainText "PLS" Nothing),
  ((V2 (V2 1 2) (V2 2 3)),  mkActionText (GameAction $ switchTo testMenu2) "TEST !" (Just 23)),
  ((V2 (V2 1 2) (V2 1 2)),  mkPlainText "YEAH !" (Just 50))
  ]

testMenu2 :: GameEnv -> IO (GMenu)
testMenu2 = basicMenu [
  ((V2 (V2 0 1) (V2 0 1)),  mkPlainText "second" Nothing),
  ((V2 (V2 1 2) (V2 2 3)),  mkActionText (GameAction $ switchTo testMenu3) "click me" (Just 23)),
  ((V2 (V2 3 7) (V2 1 2)),  mkPlainText "ok" (Just 40))
  ]
testMenu3 :: GameEnv -> IO (GMenu)
testMenu3 = basicMenu [
  ((V2 (V2 1 6) (V2 1 6)),  mkPlainText "third" Nothing),
  ((V2 (V2 1 3) (V2 4 5)),   mkActionText (GameAction $ switchTo testMenu4) "YES !" (Just 23)),
  ((V2 (V2 6 7) (V2 1 2)),  mkPlainText "ok" (Just 40))
  ]

initOgre = do
  bntex <- err $ textureFromFile (imgPath ++ "ogre.png") Nothing
  ogranim <- initAnim' bntex (replicate 4 20)
  return ogranim


testMenu4 :: GameEnv -> IO (GMenu)
testMenu4 gEnv = do
  anim1 <- initOgre
  basicMenu [
              ((V2 (V2 1 6) (V2 1 6)),  \n -> mkPlainPic anim1 ),
              ((V2 (V2 1 3) (V2 4 5)),   mkActionText (GameAction $ switchTo testMenu) "click me" (Just 23)),
              ((V2 (V2 6 7) (V2 1 2)),  mkPlainText "ok" (Just 40))
              ] gEnv
 


basicMenu cels gEnv = do
  cels' <- mapM (\(x,y') -> do
                              y <- y'
                              return (x,y)) $ mkMenuEls cels
  initMenu Nothing Nothing cels' gEnv