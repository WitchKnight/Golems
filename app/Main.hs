{-# LANGUAGE OverloadedStrings,FunctionalDependencies,MultiParamTypeClasses,ExistentialQuantification,DeriveGeneric,TemplateHaskell, GADTs,TypeSynonymInstances, FlexibleInstances #-}
module Main where

import SFML.Audio
import SFML.Graphics
import SFML.Window
import SFML.System.Clock

import Control.Monad
import Control.Concurrent
import Control.Lens.Operators
import Data.IORef
import Linear.V2
import qualified Data.HashTable.IO as HIO

import Lib
import Animations
import Conf
import View
import GameText
import GameData
import UI
import Init
import App
import Game




main :: IO ()
main = do
  print "yey"
  let ctxSettings = Just $ ContextSettings 24 8 0 1 2 [ContextDefault]
  wnd <- createRenderWindow (VideoMode gWIDTH gHEIGHT 32) "Golems" [SFDefaultStyle] ctxSettings
  clock <- createClock
  gE <- startEnv
  menu <- testMenu gE
  t <- newIORef timeZero
  let gS = InMenu wnd  gE (MenuS menu t Nothing)
  loop clock timeZero gS
  cleanGame gS
  return ()

loop :: Clock -> Time -> GameS a ->  IO ()
loop clock dt gS@(InMenu wnd env scene) = do
  dt    <- restartClock clock
  updateGame dt gS
  drawGame gS
  evt   <- pollEvent wnd
  case evt of
    Just SFEvtClosed -> return ()
    Just (SFEvtMouseMoved x y) -> do
      s <- updateCursor x y scene
      loop clock dt (withSc s gS)
    Just (SFEvtMouseButtonPressed p x y) -> do
      gS <- confirmG gS
      loop clock dt gS
    _                -> loop clock dt gS
