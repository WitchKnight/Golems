{-# LANGUAGE OverloadedStrings,FunctionalDependencies,MultiParamTypeClasses,ExistentialQuantification,DeriveGeneric,TemplateHaskell, GADTs,TypeSynonymInstances, FlexibleInstances #-}
module App where

import SFML.Audio
import SFML.Graphics
import SFML.Window
import SFML.System.Time

import GHC.Generics

import Control.Lens hiding (transform)
import Control.Monad
import Control.Concurrent.Async
import Control.Concurrent.MVar
import Linear.V2

import Foreign.Ptr (nullPtr)
import SFML.SFException

import qualified Data.HashMap.Strict as DHS
import Data.HashMap.Strict((!))
import qualified Data.HashTable.IO as HIO
-- import Data.Text
import Data.Hashable

import Data.IORef

import Lib
import Conf
import View
import GameText
import GameData
import Animations
import UI
import Game



data MenuS = MenuS {
  _sMenu    :: GMenu, 
  _msTime   :: IORef Time,
  _sCursor  :: Maybe (V2 Int)
}



type GMenu = Menu (GameS MenuS) MenuS
type GUI   = UIElem (GameS MenuS) MenuS

data GameS a where
  InMenu :: RenderWindow -> GameEnv -> MenuS -> GameS MenuS



instance Scene MenuS where
  getTimeRef (MenuS _ t _) =  t
  setTime tn (MenuS _ t _) = writeIORef t tn
  drawWithEnv tg env s@(MenuS menu t _) = do
    HIO.mapM_ f (menu^.mElems)
    where
      f (crd,el) =  do
        time <- getTime s
        print time
        let V2 x y = getUIPos crd 
        let transf =  (translation x y)
        (el^.uiLabel) >?> \x -> do
          t <- getText (x^.lId) (env^.gameText)
          t >?> drawWithTransform tg transf
        dwtat tg time transf el
    
makeLenses ''MenuS




instance GameState (GameS MenuS) MenuS where
  envG (InMenu _ env _) = env
  scenG (InMenu _ _ s)  = s
  wndG (InMenu w env scene) = w
  withSc s (InMenu w env scene) = (InMenu w env s)

  
cancelG :: (GameS MenuS) -> IO ()
cancelG g@(InMenu w env (MenuS m t _)) = do
  (m^.cancelAct) >?> execAction g

confirmG :: (GameS MenuS) -> IO (GameS MenuS)
confirmG g@(InMenu w env s@(MenuS m t _)) = do
  elems <- HIO.toList $ m^.mElems
  let activeElems = (filter (^.uiActive) $ map snd elems)
  let filtSel = \el@UIElem{uiSelect = f} ->  f s
  selecElems <- filterM filtSel activeElems
  nG <- case selecElems of
    [] ->(m^.defaultAct) >?>= g $ mapAction g
    _  -> return g
  let knot g el@UIElem{uiAction = a} = a >?>= g $ mapAction g
                                               
  cG <- foldM knot nG selecElems 
  return cG
  
switchTo :: (GameEnv -> IO GMenu) -> GameS MenuS -> IO (GameS MenuS) 
switchTo mkMenu (InMenu w gEnv menuS)= do
  print "changing menu"
  let oldMenu = menuS^.sMenu
  cleanMenu oldMenu gEnv
  newMenu <- mkMenu gEnv
  let nS = switchMenu newMenu menuS
  return (InMenu w gEnv nS)
  

cursorInRange crd howMuch wL s  = do
  let c = s^.sCursor
  print wL
  res <- case c of 
    Nothing  -> return False
    Just (V2 x y)  -> do
      let p = getUIPos crd
      return $ (distance (V2 (fromIntegral x-(wL)) (fromIntegral y)) p < howMuch)
  return res

updateCursor x y s =
  return $ s & sCursor .~ Just (V2 x y)

switchMenu newMenu s = s & sMenu.~ newMenu