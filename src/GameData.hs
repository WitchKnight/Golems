{-# LANGUAGE OverloadedStrings,ExistentialQuantification,DeriveGeneric,TemplateHaskell, GADTs,TypeSynonymInstances, FlexibleInstances #-}

module GameData where

import SFML.Audio
import SFML.Graphics
import SFML.Window
import SFML.System.Time

import GHC.Generics

import Control.Lens hiding (transform)
import Control.Monad
import Control.Concurrent.Async
import Control.Concurrent.MVar
import Linear.V2

import Foreign.Ptr (nullPtr)
import SFML.SFException

import qualified Data.HashMap.Strict as DHS
import Data.HashMap.Strict((!))
import qualified Data.HashTable.IO as HIO
-- import Data.Text
import Data.Hashable

import Data.IORef

import Lib
import Conf
import View
import GameText
import Animations


data GameEnv = GameEnv {
  _gameSnds    :: HIO.LinearHashTable Int Music,
  _gameTxtr    :: HIO.LinearHashTable Int Texture,
  _gameSprts   :: HIO.LinearHashTable Int Sprite,
  _gameAnims   :: HIO.LinearHashTable Int Animation,
  _gameText    :: GText
}
makeLenses ''GameEnv

startEnv = do
  snds  <- HIO.new
  txtrs <- HIO.new
  sprs  <- HIO.new
  anims <- HIO.new
  gt <- createGameText
  return $ GameEnv snds txtrs sprs anims gt

class Anim a => EnvAnim a where
  addAnim :: GameEnv -> Int -> a ->  IO ()
  addAnim' :: GameEnv -> a ->  IO Int
  addAnimJ :: GameEnv -> a ->  IO ()
  addAnimJ gEnv = discard (addAnim' gEnv)
  rmvAnim :: GameEnv -> Int -> a ->  IO ()
  

instance EnvAnim Animation where
  addAnim gEnv aId a = do
    HIO.insert (gEnv^.gameAnims) aId a 
  addAnim' gEnv a = do
    aId <- getMaxId (gEnv^.gameAnims) 
    addAnim gEnv (aId+1) a
    return (aId+1)
  rmvAnim gEnv i anim = do
    HIO.delete (gEnv^.gameAnims) i
    cleanAnim anim

cleanEnv gEnv = do
  HIO.mapM_ (\(_,x) -> destroy x) $ gEnv^.gameSnds
  HIO.mapM_ (\(_,x) -> destroy x) $ gEnv^.gameTxtr
  HIO.mapM_ (\(_,x) -> destroy x) $ gEnv^.gameSprts
  HIO.mapM_ (\(_,x) -> cleanAnim x) $  gEnv^.gameAnims
  cleanGameText $ gEnv^.gameText