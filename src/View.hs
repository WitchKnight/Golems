{-# LANGUAGE FlexibleInstances,DeriveGeneric,TemplateHaskell, GADTs,TypeSynonymInstances, FlexibleInstances #-}


module View where
  
import Linear.V2

import SFML.Audio
import SFML.Graphics
import SFML.Window
import SFML.System.Time

class Viewable a where
  drawWithTransform :: SFRenderTarget b => b -> Transform -> a -> IO ()
  drawV ::  SFRenderTarget b => b -> a -> IO ()
  drawV tg a = drawWithTransform tg idTransform a
   
class Anim a where
  drawWithTimeAndTransform :: SFRenderTarget b => b -> Time -> Transform -> a -> IO () 
  dwtat ::  SFRenderTarget b => b -> Time -> Transform -> a -> IO () 
  dwtat = drawWithTimeAndTransform


newtype VTA a = VTA a

instance Viewable a => Anim (VTA a) where
  drawWithTimeAndTransform  tg tm idTransform (VTA a) = drawWithTransform tg idTransform a 