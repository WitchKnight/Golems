module Conf where

tSZ :: Int
tSZ = 23
tSZ' :: Float
tSZ' = fromIntegral tSZ
gWIDTH::Int
gWIDTH = 1600
gWIDTH'::Float
gWIDTH' = fromIntegral gWIDTH
gHEIGHT ::Int
gHEIGHT  = 900
gHEIGHT'::Float
gHEIGHT' = fromIntegral gHEIGHT

imgPath = "assets/images/"
fontPath = "assets/fonts/"
soundPath = "assets/sound/"