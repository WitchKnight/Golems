module Lib where

import Linear.V2
import qualified Data.HashTable.IO as HIO

import SFML.Audio
import SFML.Graphics
import SFML.Window
import SFML.System.Time

import Control.Monad

import Conf

someFunc :: IO ()
someFunc = putStrLn "someFunc"

type Coord = V2 Float

flipx (V2 x y) = V2 (not x) y
flipy (V2 x y) = V2 x (not y)

centerSprite spr = do
    tr <- getTextureRect spr
    let w = iwidth tr
    let h = iheight tr
    let vec = Vec2f (fromIntegral w) (fromIntegral h) 
    setOrigin spr vec

c0 :: Coord
c0 = V2 0 0

c1 :: Coord
c1 = V2 1 1

distance (V2 x y) (V2 x1 y1) = sqrt $ (x-x1)**2 + (y-y1)**2


maybeToIO :: (a -> IO ()) -> Maybe a -> IO () 
maybeToIO f x= case x of
  Just y -> do
    f y
  Nothing -> return ()

maybeToIO' :: b -> (a -> IO b) -> Maybe a -> IO b 
maybeToIO' c f x= case x of
  Just y -> do
    f y
  Nothing -> return c

(>?>) :: Maybe a -> (a -> IO ()) -> IO ()
(>?>) x f = return x >>= maybeToIO f


tryCompute x or f  = return x >>= maybeToIO' or f
(>?>=) = tryCompute

getMaxId :: HIO.LinearHashTable Int a -> IO Int
getMaxId hashtable = do
  ll <- HIO.toList hashtable
  let ks = [k | (k,v) <- ll]
  return $ case ks of [] -> 0
                      _   -> maximum ks

discard f = \a -> f  a >> return () 

getUIPos ( V2 (V2 x1 x2) (V2 y1 y2) ) = 
  V2 (gWIDTH'*(x1/x2)) $ gHEIGHT'*y1/y2

maybeOr (Just x) y = x
maybeOr Nothing y = y