{-# LANGUAGE RecordWildCards,RankNTypes,FlexibleContexts,OverloadedStrings,ExistentialQuantification,DeriveGeneric,TemplateHaskell, GADTs,TypeSynonymInstances, FlexibleInstances #-}
module UI where

import SFML.Audio
import SFML.Graphics
import SFML.Window
import SFML.System.Time

import GHC.Generics
import Debug.Trace

import Control.Lens hiding (transform)
import Control.Monad
import Control.Concurrent.Async
import Control.Concurrent.MVar
import Linear.V2

import Foreign.Ptr (nullPtr)
import SFML.SFException

import qualified Data.HashMap.Strict as DHS
import Data.HashMap.Strict((!))
import qualified Data.HashTable.IO as HIO
-- import Data.Text
import Data.Hashable

import Data.IORef

import Lib
import Conf
import View
import GameText
import GameData
import Animations
import GameData
import Game

data UILabel = UILabel {
  _centered   :: Bool,
  _lId        :: Int, -- this is needed because of the way Gt works
  _lText      :: String,
  _lFont      :: GFont,
  _cSize      :: Maybe Int,
  _lColor     :: Maybe Color
} deriving (Eq)

makeLenses ''UILabel

mkBasLabel col sz tx n= UILabel {
  _centered = False,
  _lId      = n,
  _lText    = tx,
  _lFont    = MenuFont,
  _cSize    = sz,
  _lColor   = col
}

mkWhiteLabel = mkBasLabel Nothing

mkStdLabel = mkWhiteLabel (Just 25)

mkBlankLabel = mkStdLabel ""



addLabelText :: GText -> UILabel -> IO ()
addLabelText gT label = do
  createTxt gT  (label^.lId)
                (label^.lText)
                (label^.lFont) 
                (label^.cSize)
                (label^.lColor)
  return ()



  


data UIElem game scene = forall a b. (EnvAnim a, EnvAnim b) => UIElem {
  _uiActive   :: Bool,
  _uiLabel    :: Maybe UILabel,
  lkGrey     :: Maybe a,
  lkSele     :: Maybe b,
  _uiDesc     :: Maybe String,
  uiAction   :: Maybe (Action game),
  uiSelect   :: scene -> IO Bool
}



type UICoord = V2 Coord

type UIElems game scene = HIO.LinearHashTable UICoord (UIElem game scene)


data Menu game scene =  Menu {
  _mElems       :: UIElems game scene,
  _cancelAct    :: Maybe (Action game),
  _defaultAct   :: Maybe (Action game)
}
makeLenses ''UIElem
makeLenses ''Menu


centeredEl uiEl = uiEl & uiLabel._Just %~ \l -> l  & centered .~ True

getTextIds menu = do
  l <- HIO.toList $ menu^.mElems
  let mlabs = [el^.uiLabel | (_,el) <- l ]
  let   ids = [ l^.lId | (Just l) <- mlabs]
  return ids


initMenu cancel def cmElems gEnv = do
  forM_ cmElems  $ \(_,el@(UIElem { lkSele = lkS, lkGrey = lkG})) -> do
    lkG >?> addAnimJ gEnv
    lkS >?> addAnimJ gEnv
    (el^.uiLabel) >?> addLabelText (gEnv^.gameText)
  els <- HIO.fromList cmElems
  return $ Menu els cancel def

noAnim :: Maybe Animation
noAnim = Nothing


mkPlainPic :: EnvAnim a =>  a ->  IO (UIElem g s)
mkPlainPic a = return $  UIElem   False
                                  Nothing
                                  (Just a)
                                  noAnim
                                  Nothing
                                  Nothing
                                  (\x -> return $ const False x)


mkPlainText :: String -> Maybe Int -> Int -> IO (UIElem g a)
mkPlainText txt sz n =do
  let lb = mkWhiteLabel sz txt  n
  return $  UIElem  False
                    (Just lb)
                    noAnim
                    noAnim
                    Nothing
                    Nothing
                    (\x -> return $ const False x)

mkActionText :: (Action a) -> String -> Maybe Int -> Int -> IO (UIElem a b)
mkActionText action txt sz n = do
  el@UIElem{..} <- mkPlainText txt sz n
  return $ UIElem{uiAction = Just action, ..} & uiActive .~ True

  

cleanMenu :: Menu g s-> GameEnv -> IO ()
cleanMenu menu env = do
  print "cleaning menu"
  ids <- getTextIds menu
  mapM_ (\k -> if k `elem` ids then trace "deleting key" $ traceShow k $ deleteTxt k (env^.gameText) else return ()) ids


instance Anim (UIElem g a) where
  drawWithTimeAndTransform tg tm transf uiEl@(UIElem { lkSele = lkS, lkGrey = lkG}) = case uiEl^.uiActive of
    True -> lkS >?> dwtat tg tm transf 
    False -> lkG >?> dwtat tg tm transf

